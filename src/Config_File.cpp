/*
Copyright 2018 Aniket Chatterjee

    This file is a part of Spieler

    Spieler is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "Config_File.h"
#include "Contains.h"
#include <fstream>

Config_File::Config_File(std::filesystem::path location){
	this->location = location;
}

void Config_File::add_option(std::string identifier, std::string& variable){
	options[identifier] = &variable;
}

void Config_File::source(){	
	static constexpr std::array<char, 3> ignored_chars {' ', '\t', '"'};
	std::ifstream config_file (location);
	if(!config_file.is_open()){
		throw std::runtime_error("Could not find configuration file (should be located at " + location.string());
	}
	std::string line;
	int line_number = 0;
	while(getline(config_file,line)){
		line_number++;
		std::string var;
		std::string value;
		int count = 0;
		for(char i : line){
			if(i == '='){
				break;
			}
		if(!contains(ignored_chars, i)) var += i;
			count++;
		}
		for(const char& i : line.substr(count+1)){
			value += i;	
		}
		try{
			*options.at(var) = value;
		}
		catch(...){
			throw std::runtime_error("Error parsing configuration file on line " + std::to_string(line_number));
		}
	}
}
