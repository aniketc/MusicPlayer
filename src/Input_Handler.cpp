/*
Copyright 2018 Aniket Chatterjee

    This file is a part of Spieler

    Spieler is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Input_Handler.h"
#include "Player.h"
#include <vector>
#include <string>

constexpr char copyright_notice[] = 	 "Spieler\n"
					 "Copyright (C) 2018 Aniket Chatterjee\n"
					 "This program comes with ABSOLUTELY NO WARRANTY; for details type 'show w'\n"
					 "This is free software, and you are welcome to redistribute it\n"
					 "under certain conditions; type 'show c' for details.\n";

constexpr char warranty_notice[]  =	 "This program is distributed in the hope that it will be useful,\n"
				         "but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
				         "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
				         "GNU General Public License for more details.\n"
				         "\n"
				         "You should have received a copy of the GNU General Public License\n"
				         "along with this program. If not, see <https://www.gnu.org/licenses/>\n";

constexpr char freedom_notice[]   =	 "This program is free software; you can redistribute it and/or modify"
				         "it under the terms of the GNU General Public License as published by\n"
				         "the Free Software Foundation; either version 3 of the License , or\n"
				         "(at your option) any later version.\n";


Input_Handler::Input_Handler()
	:player()
{
}

bool is_number(std::string string){
	if (string.size() == 0) return false;	
	for(const char& i : string){
		if(!isdigit(i)){
			return false;
		}
	}
	return true;
}

int Input_Handler::process_input_loop(){
	std::cout << copyright_notice << '\n';
	handle_ls();
        while (std::cin){
                std::string command;
                std::cin >> command;
		if(is_number(command)){
			handle_number(std::stoi(command));
		}
		else if (command == "play"){
			handle_play();
                }
                else if(command == "pause"){
                        player.pause();
                }
                else if(command == "resume"){
                        player.resume();
                }
                else if(command == "volume"){
                }
                else if(command == "ls"){
			handle_ls();
                }
                else if(command == "cd"){
                       	handle_cd(); 
			                }
                else if(command == "pwd"){
			std::cout << player.pwd() << '\n';
                }
		else if(command == "show"){
			handle_show();
		}
                else if(command == "help"){
			handle_help();
                }
                else if(std::cin){
                        getline(std::cin, command);
                        std::cout << "Invalid Command!\n";
                }
        }
	return 0;	
}

void Input_Handler::handle_play(){
	std::string file;
	std::cin >> std::ws;
	getline(std::cin, file);
	if(!player.playInput(file)){
		std::cout << "Error loading the file\n";
	}
}

void Input_Handler::handle_ls(){
	int count = 1;
	std::cout << "0. ../" << '\n';
	for(const std::filesystem::path& i : player.ls()){
		std::cout << count << ". " <<  i.filename() << '\n';
		count++;
	}
}

void Input_Handler::handle_cd(){
	std::string dir;
	std::cin >> std::ws;
	getline(std::cin, dir);
	if(!player.cd(dir)){
		std::cout << "Invalid directory!" << '\n'; 	
	}
}

void Input_Handler::handle_pwd(){
	std::cout << player.pwd();
}

void Input_Handler::handle_number(int command){
	if(command == 0){
		player.cd("../");
		handle_ls();
		return;
	}

	command = command-1;
	std::vector<std::filesystem::path> files = player.ls();
	if(command>=files.size()) {
		std::cout << "Invalid number!\n";
		return;
	}
	if(is_directory(files.at(command))){
		player.cd(files.at(command));
		handle_ls();
	}
	else if(is_regular_file(files.at(command))){
		if(!player.playInput(files.at(command))){
			std::cout << "Error playing file!\n";
		}
	}
}

void Input_Handler::handle_help(){
	std::cout << "Type the number of a file to change to it if it's a directory, and to play it if it's a music file\n";
	std::cout << "Use ls to list all files in the directory, cd to change by directory name, and pwd to print the active directory\n";
	
}

void Input_Handler::handle_show(){
	std::string command;
	std::cin >> std::ws;
	getline(std::cin, command);
	if(command == "w"){
		std::cout << warranty_notice << '\n';
	}
	else if(command == "c"){
		std::cout << freedom_notice << '\n';
	}
	else
		std::cout << "Invalid 'show' command!\n";
}
