/*
Copyright 2018 Aniket Chatterjee

    This file is a part of Spieler

    Spieler is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Player.h"
#include <iostream>

class Input_Handler{
	public:
		Input_Handler();
		int process_input_loop();
	private:
		void handle_play();
		void handle_pause();
		void handle_resume();
		void handle_volume();
		void handle_ls();
		void handle_cd();
		void handle_pwd();
		void handle_help();
		void handle_number(int command);
		void handle_show();

		Player player;
};
