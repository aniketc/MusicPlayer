/*
Copyright 2018 Aniket Chatterjee

    This file is a part of Spieler

    Spieler is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Player.h"
#include "Contains.h"
#include "Config_File.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>
#include <iostream>
#include <pwd.h>
#include <unistd.h>
#include <algorithm>

std::string get_username()
{
  uid_t uid = geteuid();
  struct passwd *pw = getpwuid(uid);
  if (pw != nullptr)
  {
    return pw->pw_name;
  } 
  return "";
}

bool directory_exists(const std::filesystem::path& filepath){
	return std::filesystem::exists(filepath) && std::filesystem::is_directory(filepath);
}

std::string dir_to_string(std::string dirpath){
	std::string stat_dirpath;
	int pos = 0;
	for(const char& i : dirpath){
		if(i == '~' && pos == 0) {
			stat_dirpath += "/home/" + get_username();	
		}
		else if ((pos == 0 || pos == dirpath.size()-1 || dirpath.at(pos-1) == '/' || dirpath.at(pos+1) == '/') && (i=='\'' || i=='"')) {}
		else 	     		stat_dirpath += i;
		pos++;
	}
	return stat_dirpath;
}

Player::Player(){
	std::string stringdir;
	Config_File config(dir_to_string("~/.config/musicplayer.conf"));
	config.add_option("music_directory", stringdir);
	config.source();
	music_dir = dir_to_string(stringdir);
	music_dir = music_dir.lexically_normal();
	playing = nullptr;
	init();
}

void Player::set_music_dir(std::string music_dir){
	if(directory_exists(music_dir)){
	this->music_dir = std::move(music_dir);
	}
}

bool Player::init(){

	if(SDL_Init(SDL_INIT_AUDIO) < 0)
		throw std::runtime_error ("Audio initialization failed!");
	if(Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0){
		throw std::runtime_error ("Mixer initialization failed!");
	       	return false;	
	}
	return true;

}

bool Player::play(const std::string& file){
	playing = std::move(std::unique_ptr<Mix_Music, MusicDestructor> (Mix_LoadMUS(dir_to_string(file).c_str())));
	if (playing == nullptr){

		return false;
	}
	Mix_PlayMusic(playing.get(), 1);
	return true;
}

bool Player::playInput(const std::string& file){
	if (Mix_PlayingMusic() != 0){
		halt();
	}
	return play(music_dir / file);
}

bool Player::changeVolume(int amount){
	
	Mix_Volume(-1,Mix_Volume(-1,-1)+amount);
	return true;

}

void Player::pause(){

	Mix_PauseMusic();

}
void Player::resume(){

	Mix_ResumeMusic();

}

void Player::halt(){

	Mix_HaltMusic();
}

bool sort_files(std::filesystem::path one, std::filesystem::path two){
	std::string one_name = one.filename();
	std::string two_name = two.filename();
	if (isdigit(one_name.at(0)) && isdigit(two_name.at(0))){
		return stoi(one_name)<stoi(two_name);
	}
	else if (isdigit(one_name.at(0)) != isdigit(two_name.at(0))){
		return isdigit(one_name.at(0));	
	}
	else{
		return one_name<two_name;
	}
}

std::vector<std::filesystem::path> Player::ls(){
	std::vector<std::filesystem::path> files;
	std::copy(std::filesystem::directory_iterator(music_dir), 
		  std::filesystem::directory_iterator(), std::back_inserter(files));

	std::sort(files.begin(), files.end(), sort_files);
	return files;
}



bool Player::cd(const std::string& directory){
	if (directory_exists(dir_to_string((music_dir /  directory)))){
		music_dir /=  directory;	
	}	
	else{
		return false;
	}
	music_dir = music_dir.lexically_normal();
	music_dir = dir_to_string(music_dir);
	return true;
}

std::string Player::pwd(){
	return music_dir;
}
