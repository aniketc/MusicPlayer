/*
Copyright 2018 Aniket Chatterjee

    This file is a part of Spieler

    Spieler is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef _PLAYER_H
#define _PLAYER_H

#include <SDL2/SDL.h> 
#include <SDL2/SDL_mixer.h>
#include <memory>
#include <string>
#include <filesystem>
#include <vector>

struct MusicDestructor{
	void operator()(Mix_Music* music) {Mix_FreeMusic(music);}
};

class Player{
	public:
		Player();

		void set_music_dir(std::string music_dir);
		bool play(const std::string& file);
		void create_playlist(std::filesystem::path songs);
		void play_playlist(std::string name);
		void delete_playlist(std::string name);
		void pause();
		void halt();
		void resume();
		bool init();
		bool playInput(const std::string& file);
		bool changeVolume(int amount);
		bool read_config(const std::string& config_file_name);
		std::vector<std::filesystem::path> ls();
		bool cd(const std::string& directory);
		std::string pwd();
	private:
		std::unique_ptr<Mix_Music, MusicDestructor> playing;
		std::filesystem::path music_dir;
};

#endif
