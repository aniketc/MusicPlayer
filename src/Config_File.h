/*
Copyright 2018 Aniket Chatterjee

    This file is a part of Spieler

    Spieler is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <map>
#include <filesystem>
#include <string>

class Config_File{
	public:
		Config_File(std::filesystem::path location);
		void add_option(std::string identifier, std::string& variable);
		void source();
	private:
		std::map<std::string, std::string*> options;
		std::filesystem::path location;
};
